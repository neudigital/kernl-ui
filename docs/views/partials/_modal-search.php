<div class="modal --lg"
     id="modal_search"
     tabindex="-1"
     role="dialog"
     aria-labelledby="modal_search_label"
     aria-hidden="true">
  <div class="__screen" data-dismiss="modal"></div>
  <div class="__content w--100@d">
    <div class="__body">
      <form action="" method="">
        <div class="form__enclosed --search --dark --line">
          <label for="exampleText" class="sr--only">Search</label>
          <input class="fs--d3 fw--300" type="search" id="modalSearch" aria-describedby="textHelp" placeholder="Search for meaning...">
          <button type="submit" class="btn">Go</button>
        </div>
        <div class="__group --check --inline">
          <label class="__check__label mr--1">
            <input type="radio" name="opt">
            Check me out
          </label>
          <label class="__check__label">
            <input type="radio" name="opt">
            Check me out
          </label>
        </div>
      </form>
    </div>
  </div>
  <button type="button" class="__close" data-dismiss="modal" aria-label="Close">
    <i data-feather="x"></i>
  </button>
</div>
