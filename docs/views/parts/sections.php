<!-- ADD Header-FIXED TO THIS PAGE, REQUIRING 16X9 -->

<header class="section">
  <h2 class="k_section__header">Sections &amp; Headers</h2>
</header>

<section class="section --hero bg--img">
  <div class="__header">
    <h1 class="__title">Hero</h1>
    <div class="__subtitle">Includes Background Image</div>
    <p>.flex--middle on section, __body has defined width</p>
  </div>
</section>

<section class="section --banner vh--100 bg--img --bal">
  <div class="__header">
    <div class="__pretitle"><a href="#">Header Pre Title</a></div>
    <h1 class="__title">Header: 100vh, Default (Middle) Position</h1>
    <div class="__subtitle">Header Sub Title Cras justo odio, dapibus ac facilisis in, egestas eget quam</div>
  </div>
</section>

<section class="section --banner --top vh--70 bg--img">
  <div class="__header">
    <div class="__pretitle">Header Pre Title</div>
    <h1 class="__title">Header: 70vh, Top Position</h1>
    <div class="__subtitle">Header Sub Title Cras justo odio, dapibus ac facilisis in, egestas eget quam</div>
  </div>
</section>

<section class="section --banner vh--80 --bottom bg--img">
  <div class="__header">
    <div class="__pretitle">Header Pre Title</div>
    <h1 class="__title">Header: Bottom Position, 80vh</h1>
    <div class="__subtitle">Header Sub Title Cras justo odio, dapibus ac facilisis in, egestas eget quam</div>
  </div>
</section>

<section class="section --banner bg--fixed vh--60 bg--img">
  <div class="__header">
    <div class="__pretitle">Header Pre Title</div>
    <h1 class="__title">Header: Large, Fixed, 60vh</h1>
    <div class="__subtitle">Header Sub Title Cras justo odio, dapibus ac facilisis in, egestas eget quam</div>
  </div>
</section>
