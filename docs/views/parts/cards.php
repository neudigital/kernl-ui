<section class="section --bal pt--2 col--stretch">
  <div class="row">
    <!-- basic cards -->
    <div class="col w--1/3@t">
      <article class="card">
        <a href="#" class="__link">
          <div class="__body">
            <h2 class="__title">Default Card With No Extras</h2>
            <p>This example has an excerpt of 93 characters. Etiam porta sem malesuada magna mollis euismod.</p>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/3@t">
      <article class="card">
        <div class="__header pa--1">
          <div class="__column">Card Header</div>
        </div>
        <div class="__body mt--1">
          <h2 class="__title">Short Title, No Wrapping Link</h2>
          <p>This example has an excerpt of only 50 characters.</p>
        </div>
      </article>
    </div>
    <div class="col w--1/3@t">
      <article class="card">
        <a href="#" class="__link">
          <div class="__body">
            <h2 class="__title">Default Card With No Extras</h2>
            <p>This example has an excerpt of 208 characters. Etiam porta sem malesuada magna mollis euismod. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
          </div>
        </a>
      </article>
    </div>

    <!-- image example -->
    <div class="col w--1/3@d">
      <article class="card --h@t --v@d">
        <a href="#" class="__link">
          <div class="__graphic ar--16x9"></div>
          <div class="__body">
            <h2 class="__title">Card With Image and Footer</h2>
            <p>This example has an excerpt of 93 characters. Etiam porta sem malesuada magna mollis euismod.</p>
          </div>
          <div class="__footer">
            <div class="__column">Footer Text</div>
            <div class="__column"><i data-feather="arrow-right" class="--sm"></i></div>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/3@d">
      <article class="card --h@t --v@d --right">
        <a href="#" class="__link">
          <div class="__graphic ar--16x9"></div>
          <header class="__header">
            <div class="__column">
              <div class="badge">Badge</div>
            </div>
          </header>
          <div class="__body">
            <h2 class="__title">Image On Right Below Desktop</h2>
            <p>This example has an excerpt of only 50 characters.</p>
          </div>
          <div class="__footer">
            <div class="__column">Footer Text</div>
            <div class="__column"><i data-feather="arrow-right" class="--sm"></i></div>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/3@d">
      <article class="card --h@t --v@d">
        <div class="__graphic ar--16x9"></div>
        <div class="__body">
          <h2 class="__title">This Card Has an Image and an Exceptionally Lengthy Title and No Wrapping Link</h2>
          <p>This example has an excerpt of 208 characters. Etiam porta sem malesuada magna mollis euismod. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
        </div>
        <div class="__footer">
          <div class="__column">Footer Text</div>
          <div class="__column"><i data-feather="arrow-right" class="--sm"></i></div>
        </div>
      </article>
    </div>

    <!-- mixed ratios and orientations at multiple breakpoints -->
    <div class="col w--1/2@t w--1/4@d">
      <article class="card checkpoint" data-checkpoint-animation="fade-up">
        <a href="#" class="__link">
          <div class="__graphic ar--1x1 ar--16x9@w"></div>
          <div class="__body">
            <h2 class="__title">Card With Image and Footer</h2>
            <p>This example has an excerpt of 93 characters. Etiam porta sem malesuada magna mollis euismod.</p>
          </div>
          <div class="__footer">
            <div class="__column">Footer Text</div>
            <div class="__column"><i data-feather="arrow-right" class="--sm"></i></div>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/2@t w--1/4@d">
      <article class="card checkpoint --h --right --v@t" data-checkpoint-animation="fade-up">
        <a href="#" class="__link">
          <div class="__graphic ar--16x9 ar--1x1@t ar--16x9@w"></div>
          <div class="__body">
            <h2 class="__title">Image On Right Below Tablet</h2>
            <p>This example has an excerpt of only 50 characters.</p>
          </div>
          <div class="__footer">
            <div class="__column">Footer Text</div>
            <div class="__column"><i data-feather="arrow-right" class="--sm"></i></div>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/2@t w--1/4@d">
      <article class="card checkpoint" data-checkpoint-animation="fade-up">
        <a href="#" class="__link">
          <div class="__graphic ar--16x9 ar--1x1@d ar--16x9@w"></div>
          <div class="__body">
            <h2 class="__title">This Card Has an Image and an Exceptionally Lengthy Title</h2>
            <p>This example has an excerpt of 208 characters. Etiam porta sem malesuada magna mollis euismod. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
          </div>
          <div class="__footer">
            <div class="__column">Footer Text</div>
            <div class="__column"><i data-feather="arrow-right" class="--sm"></i></div>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/2@t w--1/4@d">
      <article class="card checkpoint" data-checkpoint-animation="fade-up">
        <a href="#" class="__link">
          <div class="__graphic ar--16x9 ar--1x1@d ar--16x9@w"></div>
          <div class="__body">
            <h2 class="__title">This Card Has an Image and an Exceptionally Lengthy Title</h2>
            <p>This example has an excerpt of 93 characters. Etiam porta sem malesuada magna mollis euismod.</p>
          </div>
          <div class="__footer">
            <div class="__column">Footer Text</div>
            <div class="__column"><i data-feather="arrow-right" class="--sm"></i></div>
          </div>
        </a>
      </article>
    </div>

    <!-- Profile -->

    <div class="col w--1/2 w--1/3@t w--20@d">
      <article class="card --profile">
        <a href="#" class="__link">
          <div class="__graphic ar--1x1"></div>
          <div class="__body py--1">
            <h2 class="__title">Kay Rooney</h2>
            <div class="__subtitle">School of HummaJamma BoobaDooda, University of Edinburgh</div>
            <p>General Educational Development (GED) Examiner</p>
          </div>
          <div class="__footer">
            <div class="__column"><i data-feather="more-vertical" class="--sm"></i>View Profile</div>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/2 w--1/3@t w--20@d">
      <article class="card --profile">
        <a href="#" class="__link">
          <div class="__graphic ar--1x1"></div>
          <div class="__body py--1">
            <h2 class="__title">Cooper O'Neill</h2>
            <div class="__subtitle">School of HummaJamma BoobaDooda, Boston College</div>
            <p>Individualized Education Plan (IEP) Aide</p>
          </div>
          <div class="__footer">
            <div class="__column"><i data-feather="more-vertical" class="--sm"></i>View Profile</div>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/2 w--1/3@t w--20@d">
      <article class="card --profile">
        <a href="#" class="__link">
          <div class="__graphic ar--1x1"></div>
          <div class="__body py--1">
            <h2 class="__title">Lillie Squires (Lil)</h2>
            <div class="__subtitle">School of HummaJamma BoobaDooda, MIT</div>
            <p>Assistant Professor, Communication Studies and Faculty Affiliate, Women’s, Gender, &amp; Sexuality Studies Program and Department of Cultures, Societies, &amp; Global Studies</p>
          </div>
          <div class="__footer">
            <div class="__column"><i data-feather="more-vertical" class="--sm"></i>View Profile</div>
           </div>
        </a>
      </article>
    </div>
    <div class="col w--1/2 w--1/3@t w--20@d">
      <article class="card --profile">
        <a href="#" class="__link">
          <div class="__graphic ar--1x1"></div>
          <div class="__body py--1">
            <h2 class="__title">Millie Harper</h2>
            <div class="__subtitle">School of HummaJamma BoobaDooda, Northeastern University</div>
            <p>Scholastic Aptitude Test (SAT) Grader</p>
          </div>
          <div class="__footer">
            <div class="__column"><i data-feather="more-vertical" class="--sm"></i>View Profile</div>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/2 w--1/3@t w--20@d">
      <article class="card --profile">
        <a href="#" class="__link">
          <div class="__graphic ar--1x1"></div>
          <div class="__body py--1">
            <h2 class="__title">Elizabeth Stuart (Liza)</h2>
            <div class="__subtitle">College of Computer and Information Science, Northeastern University</div>
            <p>Associate Dean of Academic Affairs, Diversity and Inclusion and Dean's Professor of Civic Sustainability, College of Social Sciences and Humanities</p>
          </div>
          <div class="__footer">
            <div class="__column"><i data-feather="more-vertical" class="--sm"></i>View Profile</div>
           </div>
        </a>
      </article>
    </div>

    <!-- image overlay example -->

    <div class="col w--1/3@d">
      <article class="card --overlay">
        <a href="#" class="__link">
          <div class="__graphic"></div>
          <header class="__header">
            <div class="__column">
              <div class="badge">Badge</div>
            </div>
          </header>
          <div class="__body">
            <h2 class="__title">Default Overlay Card</h2>
            <p>This example has an excerpt of 208 characters. Etiam porta sem malesuada magna mollis euismod. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/3@d">
      <article class="card --overlay --middle">
        <div class="__graphic"></div>
        <header class="__header">
          <div class="__column">
            <div class="badge">Badge</div>
          </div>
        </header>
        <div class="__body">
          <h2 class="__title">Overlay Middle, No Wrapping Link</h2>
          <p>This example has an excerpt of only 50 characters.</p>
        </div>
      </article>
    </div>
    <div class="col w--1/3@d">
      <article class="card --overlay --bottom">
        <a href="#" class="__link">
          <div class="__graphic"></div>
          <div class="__body">
            <h2 class="__title">Overlay Bottom Bibendum Elit Risus Adipiscing Tortor</h2>
            <p>This example has an excerpt of 93 characters. Etiam porta sem malesuada magna mollis euismod.</p>
          </div>
        </a>
      </article>
    </div>

    <!-- tile option -->

    <div class="col w--1/2 w--1/3@t w--1/6@d">
      <article class="card --tile">
        <a href="#" class="__link">
          <div class="__body">
            <h2 class="__title">Default Tile</h2>
            <p>This example has an excerpt of only 50 characters.</p>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/2 w--1/3@t w--1/6@d">
      <article class="card --tile --middle">
        <a href="#" class="__link">
          <div class="__body">
            <h2 class="__title">Middle Tile Title</h2>
            <p>This example has an excerpt of 93 characters. Etiam porta sem malesuada magna mollis euismod.</p>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/2 w--1/3@t w--1/6@d">
      <article class="card --tile">
        <a href="#" class="__link">
          <div class="__body">
            <h2 class="__title">Default Tile</h2>
            <p>This example has an excerpt of 208 characters. Etiam porta sem malesuada magna mollis euismod. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/2 w--1/3@t w--1/6@d">
      <article class="card --tile --bottom">
        <a href="#" class="__link">
          <div class="__body">
            <h2 class="__title">Bottom Tile</h2>
            <p>This example has an excerpt of 93 characters. Etiam porta sem malesuada magna mollis euismod.</p>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/2 w--1/3@t w--1/6@d">
      <article class="card --tile">
        <a href="#" class="__link">
          <div class="__body">
            <h2 class="__title">Default Tile With a Significantly Longer Title</h2>
            <p>This example has an excerpt of 208 characters. Etiam porta sem malesuada magna mollis euismod. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
          </div>
        </a>
      </article>
    </div>
    <div class="col w--1/2 w--1/3@t w--1/6@d">
      <article class="card --tile --middle">
        <a href="#" class="__link">
          <div class="__body">
            <h2 class="__title">Middle Tile Title</h2>
            <p>This example has an excerpt of only 50 characters.</p>
          </div>
        </a>
      </article>
    </div>

  </div>
</section>
